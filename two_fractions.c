#include<stdio.h>
struct fract
{
    int  num;
    int  den;
};

typedef struct fract fraction;

fraction input()
{
    fraction a;
    printf("Enter the numerator: \n");
    scanf("%d",&a.num);
    printf("Enter the denominator: \n");
    scanf("%d",&a.den);
    return a; 
}

int  find_gcd(int a, int b)
{
    int i, gcd;
    for(i=1; i<=a && i<=b; i++)
    {
        if(a%i==0 && b%i==0)
        gcd=i;
        
    }
    return gcd;
} 

fraction sum(fraction a,fraction b)
{
    int gcd;
    fraction res;
    if(a.den==b.den)
    {
        res.den=a.den;
        res.num=(a.num+b.num);
        
    }
    else 
    {
        res.den=(a.den*b.den);
        res.num=(a.num*b.den)+(b.num*a.den);
    }
    gcd=find_gcd(res.num, res.den);
    res.num /= gcd;
    res.den /= gcd;
    return res;
}

int main()
{
    fraction a,b,c;
    a=input();
    b=input();
    c=sum(a,b);
    printf("The final sum is %d/%d",c.num,c.den);
    return 0;
}