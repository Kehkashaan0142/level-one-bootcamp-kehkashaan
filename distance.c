
#include <stdio.h>
#include <math.h>
int inputa()
{
    float a;
    scanf("%f",&a);
    return a;
}

int inputb()
{
    float b;
    scanf("%f",&b);
    return b;
}
void print (float dist)
{
    printf("Distance = %f\n",dist);
}
float calc(float a1, float b1,float a2,float b2)
{
     return (sqrt(( (a2-a1)*(a2-a1) )+( (b2-b1)*(b2-b1) )));
}
int main() 
{
    float a1, b1, a2, b2;    
    printf("Input a1: \n");
    a1=inputa();   
    printf("Input b1: \n");
    b1=inputb();   
    printf("Input a2: \n");
    a2=inputa();    
    printf("Input b2: \n");
    b2=inputb();    
    float dist = calc(a1,b1,a2,b2);
    print(dist);
    return 0;
}